const { Builder, By, Key, until } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/chrome");
const chromedriver = require("chromedriver");
const assert = require("assert");

// function sleep(ms) {
//   return new Promise(resolve => setTimeout(resolve, ms));
// }

describe("Beyond Exams website", function () {
  let driver;

  before(async function () {
    driver = await new Builder().forBrowser("chrome").build();
  });

  after(async function () {
    this.timeout(3000);
    await driver.quit();
  });

  it("should have the correct title", async function () {
    this.timeout(30000);
    await driver.get("https://beyondexams.org/");
    const title = await driver.getTitle();
    console.log(title);
    assert.strictEqual(title, "BeyondExams");
  });

  it('should have a logo', async function() {
    this.timeout(30000);
    await driver.get('https://beyondexams.org/');
    const logo = await driver.findElement(By.className('nav-logo-m')).isDisplayed();
    console.log('Logo displayed: ' + logo);
    assert.ok(logo, 'Logo not displayed');
  });
  
  it('should have a search bar', async function() {
    this.timeout(30000);
    await driver.get('https://beyondexams.org/');
    const searchBar = await driver.findElement(By.className('Nav-search')).isDisplayed();
    console.log('Search bar displayed: ' + searchBar);
    assert.ok(searchBar, 'Search bar not displayed');
  });

  it('Should search for and display results', async function () {
    this.timeout(30000);
    const searchInput = await driver.findElement(By.className('home-search-input'));
    await searchInput.sendKeys('computer', Key.RETURN);
    await driver.wait(until.elementLocated(By.className('search_mid')), 100000);
    const results = await driver.findElement(By.className('search_mid')).isDisplayed();
    assert.ok(results, 'Search results not displayed');
  });

  it("Should allow user to sign up using Google account", async function () {
    this.timeout(300000);
    try {
      // Navigate to the signup page
      await driver.get("https://beyondexams.org/login");

      // Click on the "Sign up with Google" button
      await driver.findElement(By.className("SelectorLoginBTN")).click();

      // Switch to the Google sign-in popup window
      const handles = await driver.getAllWindowHandles();
      await driver.switchTo().window(handles[1]);

      // Enter valid Google account credentials and sign in
      await driver.wait(until.elementLocated(By.id("identifierId")), 50000);
      await driver.findElement(By.id("identifierId")).sendKeys("example@gmail.com");
      await driver.findElement(By.id("identifierNext"), 100000).click();
      await driver.wait(until.elementLocated(By.name("password")), 100000);
      await driver.findElement(By.name("password")).sendKeys("123");
      await driver.findElement(By.id("passwordNext")).click();


      // Switch back to the Beyond Exams window
      await driver.switchTo().window(handles[0]);

      // Wait for the login as student option
      await driver.wait(
        until.elementLocated(By.className("btn-selector-p-2")),
        10000
      );
      // Select the login option as a student
      const loginOption = await driver.findElement(
        By.className("btn-selector-p-2")
      );
      if ((await loginOption.getText()) === "Student") {
        await loginOption.click();
      }
      // Wait for the login to complete and the user to be redirected to the beyondexams page
      await driver.wait(until.urlContains("beyondexams"), 50000);

      // Verify that the URL contains the beyondexams
      const url = await driver.getCurrentUrl();
      assert.ok(url.includes("beyondexams"), "Login was not successful");
    } catch (error) {
      console.error(error);
      assert.fail(error);
    }
  });
});
